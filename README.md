An empty android project template in Model-View_ViewModel architecture
Language: Kotlin
List of Libraries:
1) Dagger 2 Android -> for Dependency Injection
2) LifeCycle -> for mvvm arch, view models
3) RxJava 3 -> for reactive flows
4) Room -> for persistence database
5) Navigation component -> Single Activity effect
6) Glide -> for loading resources into Images
7) Retrofit 2 -> for simple network calls
8) Desugaring -> for uisng Java 8 usefull apis like Instant, Lambda ...
9) SDP, SSP -> for supporting layouts on multi device screens