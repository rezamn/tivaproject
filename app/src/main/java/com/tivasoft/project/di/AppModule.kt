package com.tivasoft.project.di

import android.app.Application
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.tivasoft.project.R
import com.tivasoft.project.data.database.MyDatabase
import com.tivasoft.project.util.network.NetworkConnectivityCheckInterceptor
import dagger.Module
import dagger.Provides
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(application: Application): Retrofit {

        val okHttpClient = OkHttpClient()
            .newBuilder()
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            .addInterceptor(NetworkConnectivityCheckInterceptor(application))
            .build()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .baseUrl("")
            .client(okHttpClient)
            .build()
        return retrofit
    }

    @Singleton
    @Provides
    fun provideRoom(application: Application): MyDatabase {
        return Room
            .databaseBuilder(
                application,
                MyDatabase::class.java,
                application.resources.getString(R.string.app_name)
            )
            .build()
    }

    @Singleton
    @Provides
    fun provideGlide(application: Application): RequestManager = Glide.with(application)

}