package com.tivasoft.project.di.main.host

import androidx.lifecycle.ViewModel
import com.tivasoft.project.di.ViewModelKey
import com.tivasoft.project.ui.host.HostViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class HostViewModelModule {

    @HostScope
    @Binds
    @IntoMap
    @ViewModelKey(HostViewModel::class)
    abstract fun bindHostViewModel(viewModel: HostViewModel):ViewModel
}