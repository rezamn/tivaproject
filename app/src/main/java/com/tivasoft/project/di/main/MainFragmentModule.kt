package com.tivasoft.project.di.main

import com.tivasoft.project.di.main.host.HostScope
import com.tivasoft.project.di.main.host.HostViewModelModule
import com.tivasoft.project.ui.host.HostFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentModule {

    @HostScope
    @ContributesAndroidInjector(modules = [HostViewModelModule::class])
    abstract fun contributeHostFragment():HostFragment
}