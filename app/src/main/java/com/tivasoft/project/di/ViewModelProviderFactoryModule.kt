package com.tivasoft.project.di

import androidx.lifecycle.ViewModelProvider
import com.tivasoft.project.util.ViewModelProviderFactoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class ViewModelProviderFactoryModule {

    @Singleton
    @Binds
    abstract fun bindViewModelProviderFactory(factory:ViewModelProviderFactoryImpl):ViewModelProvider.Factory
}