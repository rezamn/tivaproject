package com.tivasoft.project.di.main

import androidx.lifecycle.ViewModel
import com.tivasoft.project.di.ViewModelKey
import com.tivasoft.project.ui.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule {

    @MainScope
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}