package com.tivasoft.project.di

import com.tivasoft.project.ui.MainActivity
import com.tivasoft.project.di.main.MainFragmentModule
import com.tivasoft.project.di.main.MainModule
import com.tivasoft.project.di.main.MainScope
import com.tivasoft.project.di.main.MainViewModelModule
import dagger.Module
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module
abstract class AppActivitiesModule {

    @MainScope
    @ContributesAndroidInjector(modules = [MainModule::class,MainFragmentModule::class,MainViewModelModule::class])
    abstract fun contributeMainActivity(): MainActivity
}