package com.tivasoft.project.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tivasoft.project.data.model.TestModel

@Database(entities = [TestModel::class], version = 1, exportSchema = false)
abstract class MyDatabase: RoomDatabase() {

    abstract fun mainDao(): MainDao
}