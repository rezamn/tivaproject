package com.tivasoft.project.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TestModel (
    @PrimaryKey
    val id:Int
)