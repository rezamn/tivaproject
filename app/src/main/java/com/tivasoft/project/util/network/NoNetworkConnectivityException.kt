package com.tivasoft.project.util.network

import java.io.IOException

class NoNetworkConnectivityException : IOException("No Internet Connection")