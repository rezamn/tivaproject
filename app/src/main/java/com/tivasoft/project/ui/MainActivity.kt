package com.tivasoft.project.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.tivasoft.project.R
import com.tivasoft.project.util.ViewModelProviderFactoryImpl
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_app_bar.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactoryImpl
    lateinit var mainViewModel: MainViewModel

    lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProvider(this,viewModelProviderFactory)[MainViewModel::class.java]
        Log.d("test", "onCreate: ${mainViewModel.test}")

        handleNavigation()
    }

    /** @author Festive
     * config actionbar navigation drawer @see  with navigation component nav graph.
     * navigation view item click listener
     */
    private fun handleNavigation(){
        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment_container)
        appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        setupActionBarWithNavController(navController,appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.setNavigationItemSelectedListener {menuItem->
            drawerLayout.closeDrawer(GravityCompat.START)
            return@setNavigationItemSelectedListener when(menuItem.itemId){
                R.id.nav_view_menu_item_1->{
                    true
                }
                R.id.nav_view_menu_item_2->{
                    true
                }
                else-> true
            }
        }
    }

    /** @author Festive
     * enable toolbar back button when current fragment is not nav host fragment
     */
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_container)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}